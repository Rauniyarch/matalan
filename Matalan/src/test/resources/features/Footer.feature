Feature: Footer

Background:
Given when I am in HomePage

Scenario: Verify Store Finder link Icon on Footer 
When I click on Store finder Link on footer section and click Enter
Then I should be directed to Store Finder Page

Scenario: Verify FAQs link Icon on Footer 
When I Click on FAQs link Icon on footer section 
Then I should be directed to customer service FAQs page

Scenario: Verify Track My Order in footer Customer Services
When I click on Track My Order link
Then I should be directed to Delivery Info page

Scenario: Verify Here To Help in footer Customer Services
When I click on Here To Help link 
Then I should see Help Page

Scenario: Verify Gift Cards Link in footer Customer Services
When I click on Gift Cards Link
Then I should be directed to Gift Cards Page

Scenario: Verify Delivery link in Footer Customer Services
When I click on delivery Link 
Then I should be directed to Delivery Info page

Scenario: Verify Returns link in Footer Customer Services
When I click on Returns Link 
Then I should be directed to Returns Info page

Scenario: Verify Product Recall link in Footer Customer Services
When I Click on Product Recall Link
Then I should be directed to Product Recall Page

Scenario: Verify FAQs link on Footer Customer Services
When I click on FAQs link 
Then I should be directed to customer service FAQs page

Scenario: Verify Student Discount link on Footer Customer Services
When I click on Student Discount link
Then I should be directed to Student Discount Info page

Scenario: Verify About link on Footer About Matalan
When I click on About Link 
Then I should be directed to About Matalan Info page

Scenario: Verify Coronavirus Link on Footer About Matalan
When I click on Coronavirus link 
Then I should be directed to Coronavirus Info Page

Scenario: Verify History Link on Footer About Matalan
When I click on History link 
Then I should be directed to Matalan History Info Page 

Scenario: Verify Jobs Link on Footer About Matalan 
When I click on Jobs link
Then I should be directed to Jobs Search Page

Scenario: Verify Modern Slavery Act Link on Footer About Matalan
When I click on Modern Slavery Act link
Then I should be directed to Modern Slavery Act Statement Page

Scenario: Verify Corporate Publications Link on Footer About Matalan
When I click on Corporate Publications link
Then I should be directed to Corporate Publications Info Page

Scenario: Verify Charity Link on Footer About Matalan
When I click on Charity link 
Then I should be directed to Charity Info Page

Scenario: Verify Cookies Link on Footer Our Website
When I click on Cookies link 
Then I should be directed to Cookies Info Page

Scenario: Verify Privacy Policy Link on Footer Our Website
When I click on Privacy Policy link
Then I should be directed to Privacy Policy Info Page

Scenario: Verify Terms & Conditions Link on Footer Our Website
When I click on Terms & Condition link
Then I should be directed to Terms & Conditions Info Page

Scenario: Verify Acceptable Use Policy Link on Footer Our Website
When I click on Acceptable Use Policy link
Then I should be directed to Acceptable Use Policy Info Page

Scenario: Verify Sitemap Link on Footer Our Website
When I click on Sitemap link
Then I should be directed to Sitemap Page


