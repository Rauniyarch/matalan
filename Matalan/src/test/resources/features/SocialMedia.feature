Feature: SocialMedia

Background:
Given when I am in HomePage

Scenario: Verify Twitter Icon on Footer
When I click on Twitter Icon 
Then I should be directed to Twitter page of Matalan external Page

Scenario: Verify Instagram Icon on Footer
When I click on Instagram Icon
Then I should be directed to Instagram page of Matalan external Page

Scenario: Verify YouTube Icon on Footer
When I click on YouTube Icon
Then I should be directed to YouTube page of Matalan external Page

Scenario: Verify Facebook Icon on Footer
When I click on Facebook Icon
Then I should be directed to Facebook page of Matalan external Page

Scenario: Verify Pinterest Icon on Footer
When I click on Pinterest Icon
Then I should be directed to Pinterest page of Matalan external Page
