Feature: Header

Background:
Given when I am in HomePage

Scenario: verify logo to go to HomePage
When I click on Matalan Logo 
Then I should be in HomePage

Scenario: Verify saved icon on HomePage
When I click on saved icon
Then I should be directed to saved-items page and see saved products

Scenario: Verify Bag/Cart on HomePage
When I click View-Bag icon
Then I should be directed to cart page

Scenario: Verify My Account link on HomePage
When I click on My Account link
Then I Should be directed to Account/Login page

Scenario: Verify Delivery link on HomePage
When I Click on Delivery option Link
Then I should be directed to Delivery Info page

Scenario: Verify Returns link on HomePage
When I Click on Returns option Link
Then I should be directed to Returns Info page

Scenario: Verify Store Finder link on HomePage
When I click on Store Finder Link 
Then I should be directed to Store Finder Page

Scenario: Verify Contact link on HomePage
When I click on contact Link 
Then I should see Help Page

Scenario: Verify cookies Policy
When I click on GOT IT! Button to accept the cookies Policy
Then The popup message should disappear

Scenario: Verify disappear cookies popup box
When I click on cancel Button on the cookies Policy
Then The popup message should disappear





