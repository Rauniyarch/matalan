Feature: Signup

Background: 
Given when I am in HomePage

Scenario: Verify sign up with valid email Address
When I enter valid email address and press enter
Then I should see the confirmation box with recaptcha

Scenario: Verify sign up with invalid email Address
When I enter invalid email address and press enter
Then I should see the confirmation box with recaptcha

