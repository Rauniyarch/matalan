Feature: StoreFinder

Background:
Given when I am in HomePage
When I click on Store Finder Link 
Then I should be directed to Store Finder Page

Scenario Outline: Verify Store Finder with Valid Data
And I enter valid Detail in search field "<StoreSearchData>"
Then I should see stores near that Detail
Examples:
|StoreSearchData|
|TW3 4HX|
|Richmond|
|639|

Scenario: Verify Store Finder show more stores
When I enter valid Detail in search field "tw3" and click on show more stores 
Then I should see list of all other stores

Scenario Outline: Verify Store Finder with Invalid Data
When I enter Invalid Detail "<InvalidSearchData>"
Then I should see Error Message
Examples:
|InvalidSearchData|
|12345|
|Tw hx|
