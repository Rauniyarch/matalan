Feature: Search

Background:
Given when I am in HomePage
@tags
Scenario Outline: verify with valid Search Inputs
When I search with Valid "<SearchTerm>"
Then I should see valid "<searchResults>"

Examples:
|SearchTerm|searchResults|
|Shirt|Search Results for 'shirt' – Matalan|
|Men|Shop All Menswear Clothing Online – Matalan|
|S2796725_C211|Long Sleeve Utility Shirt – Green – Matalan|
||Online Clothes Shopping - Shop Latest Fashion – Matalan|


Scenario Outline: verify with Invalid Search Inputs
When I search with invalid "<invalidSearchTerm>"
Then I should see error message
Examples:
|invalidSearchTerm|
|abcd|
|1234|
|potato|
|abcd123@£|