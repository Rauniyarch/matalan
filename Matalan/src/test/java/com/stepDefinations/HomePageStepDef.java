package com.stepDefinations;

import com.runner.BaseClass;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class HomePageStepDef extends BaseClass{

	@Given("^when I am in HomePage$")
	public void when_I_am_in_HomePage() throws Throwable {
	   homePage.verifyHomePageLogo();
	}

	@When("^I click on Matalan Logo$")
	public void i_click_on_Matalan_Logo() throws Throwable {
	   homePage.clickHomePageLogo();
}
	
	@Then("^I should be in HomePage$")
	public void i_should_be_in_HomePage() throws Throwable {
	    homePage.verifyHomePageLogo();
	}
	
	@When("^I click on saved icon$")
	public void i_click_on_saved_icon() throws Throwable {
	    homePage.homePageSavedIcon();
	}
	
	@When("^I click View-Bag icon$")
	public void i_click_View_Bag_icon() throws Throwable {
	   homePage.viewBagLink();
	}
	
	@When("^I click on My Account link$")
	public void i_click_on_My_Account_link() throws Throwable {
	    homePage.myAccountLink();
	}
	
	@When("^I Click on Delivery option Link$")
	public void i_Click_on_Delivery_option_Link() throws Throwable {
		homePage.deliveryOptionsLink();
	}
	
	@When("^I Click on Returns option Link$")
	public void i_Click_on_Returns_option_Link() throws Throwable {
	    homePage.deliveryOptionsLink();
	}
	
	@When("^I click on Store Finder Link$")
	public void i_click_on_Store_Finder_Link() throws Throwable {
	    homePage.storeFinderLink();
	}
	
	@When("^I click on contact Link$")
	public void i_click_on_contact_Link() throws Throwable {
	    homePage.contactLink();
	}
	
	@When("^I click on GOT IT! Button to accept the cookies Policy$")
	public void i_click_on_GOT_IT_Button_to_accept_the_cookies_Policy() throws Throwable {
	    homePage.gotItCookiesPolicy();
	}
	
	@Then("^The popup message should disappear$")
	public void the_popup_message_should_disappear() throws Throwable {
		homePage.disappearCookiePopup();
	}
	
	@When("^I click on cancel Button on the cookies Policy$")
	public void i_click_on_cancel_Button_on_the_cookies_Policy() throws Throwable {
	   homePage.cancelCookiePolicy();
	}
	
	@When("^I enter valid email address and press enter$")
	public void i_enter_valid_email_address_and_press_enter() throws Throwable {
	    homePage.signUpValidEmailAddress();
	}
	
	@When("^I enter invalid email address and press enter$")
	public void i_enter_invalid_email_address_and_press_enter() throws Throwable {
		homePage.signUpInvalidEmailAddress();
	}
	
	@When("^I click on Store finder Link on footer section and click Enter$")
	public void i_click_on_Store_finder_Link_on_footer_section_and_click_Enter() throws Throwable {
	    homePage.verifyStoreFinderIconFooter();
	}
	
	@When("^I Click on FAQs link Icon on footer section$")
	public void i_Click_on_FAQs_link_Icon_on_footer_section() throws Throwable {
	    homePage.veriftFAQLinkIconFooter();
	}
	
	@When("^I click on Track My Order link$")
	public void i_click_on_Track_My_Order_link() throws Throwable {
	   homePage.verifyTrackMyOrderLink();
	}
	
	@When("^I click on Here To Help link$")
	public void i_click_on_Here_To_Help_link() throws Throwable {
	    homePage.verifyHereToHelpLink();
	}
	
	@When("^I click on Gift Cards Link$")
	public void i_click_on_Gift_Cards_Link() throws Throwable {
	    homePage.verifyGiftCardLink();
	}
	
	@When("^I click on delivery Link$")
	public void i_click_on_delivery_Link() throws Throwable {
	   homePage.verifyFooterDeliveryLink();
	}
	
	@When("^I click on Returns Link$")
	public void i_click_on_Returns_Link() throws Throwable {
	    homePage.verifyFooterDeliveryLink();
	}
	
	@When("^I Click on Product Recall Link$")
	public void i_Click_on_Product_Recall_Link() throws Throwable {
	    homePage.verifyFooterProductRecallLink();
	}
	
	@When("^I click on FAQs link$")
	public void i_click_on_FAQs_link() throws Throwable {
	    homePage.verifyFooterFAQLink();
	}
	
	@When("^I click on Student Discount link$")
	public void i_click_on_Student_Discount_link() throws Throwable {
	    homePage.verifyFooterStudentDiscountLink();
	}
	
	@When("^I click on About Link$")
	public void i_click_on_About_Link() throws Throwable {
	   homePage.verifyAboutLinkFooter();
	}
	
	@When("^I click on Coronavirus link$")
	public void i_click_on_Coronavirus_link() throws Throwable {
	    homePage.verifyCoronavirusLinkFooter();
	}
	
	@When("^I click on History link$")
	public void i_click_on_History_link() throws Throwable {
	    homePage.verifyHistoryLinkFooter();
	}
	
	@When("^I click on Jobs link$")
	public void i_click_on_Jobs_link() throws Throwable {
	    homePage.verifyJobsLinkFooter();
	}
	
	@When("^I click on Modern Slavery Act link$")
	public void i_click_on_Modern_Slavery_Act_link() throws Throwable {
	    homePage.verifyModernSlaveryActLinkFooter();
	}
	
	@When("^I click on Corporate Publications link$")
	public void i_click_on_Corporate_Publications_link() throws Throwable {
	    homePage.verifyCoPublicationsLinkFooter();
	}
	
	@When("^I click on Charity link$")
	public void i_click_on_Charity_link() throws Throwable {
	   homePage.verifyCharityLinkFooter();
	}
	
	@When("^I click on Cookies link$")
	public void i_click_on_Cookies_link() throws Throwable {
	   homePage.verifyCookiesLinkFooter();
	}
	
	@When("^I click on Privacy Policy link$")
	public void i_click_on_Privacy_Policy_link() throws Throwable {
	    homePage.verifyPrivacyPolicyLinkFooter();
	}
	
	@When("^I click on Terms & Condition link$")
	public void i_click_on_Terms_Condition_link() throws Throwable {
	    homePage.verifyTermsConditionsLinkFooter();
	}
	
	@When("^I click on Acceptable Use Policy link$")
	public void i_click_on_Acceptable_Use_Policy_link() throws Throwable {
	   homePage.verifyAcceptableUsePolicyLinkFooter();
	}
	
	@When("^I click on Sitemap link$")
	public void i_click_on_Sitemap_link() throws Throwable {
	    homePage.verifySitemapPage();
	}
	
	@When("^I click on Twitter Icon$")
	public void i_click_on_Twitter_Icon() throws Throwable {
	    homePage.verifyTwitterIcon();
	}
	
	@When("^I click on Instagram Icon$")
	public void i_click_on_Instagram_Icon() throws Throwable {
	   homePage.verifyInstagramIcon();
	}
	
	@When("^I click on YouTube Icon$")
	public void i_click_on_YouTube_Icon() throws Throwable {
	    homePage.verifyYoutubeIcon();
	}
	
	@When("^I click on Facebook Icon$")
	public void i_click_on_Facebook_Icon() throws Throwable {
		homePage.verifyFacebookIcon();
	}
	
	@When("^I click on Pinterest Icon$")
	public void i_click_on_Pinterest_Icon() throws Throwable {
	   homePage.verifyPinterestICon();
	}
	
	@When("^I search with Valid \"([^\"]*)\"$")
	public void i_search_with_Valid(String SearchTerm) throws Throwable {
	    homePage.verifySearchwithValidSearchInputs(SearchTerm);
	}
	
	@When("^I search with invalid \"([^\"]*)\"$")
	public void i_search_with_invalid(String invalidSearchTerm) throws Throwable {
	    homePage.verifySearchwithInvalidSearchInputs(invalidSearchTerm);
	}
	
	
}
