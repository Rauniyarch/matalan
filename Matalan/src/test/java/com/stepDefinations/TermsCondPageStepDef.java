package com.stepDefinations;

import com.runner.BaseClass;
import cucumber.api.java.en.Then;

public class TermsCondPageStepDef extends BaseClass{
	
	@Then("^I should be directed to Terms & Conditions Info Page$")
	public void i_should_be_directed_to_Terms_Conditions_Info_Page() throws Throwable {
		termsCondPage.verifyTermsCondPage();
	}
}
