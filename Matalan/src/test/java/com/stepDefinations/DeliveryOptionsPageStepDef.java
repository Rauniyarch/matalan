package com.stepDefinations;

import com.runner.BaseClass;

import cucumber.api.java.en.Then;

public class DeliveryOptionsPageStepDef extends BaseClass{

	@Then("^I should be directed to Delivery Info page$")
	public void i_should_be_directed_to_Delivery_Info_page() throws Throwable {
	   deliveryOptionsPage.verifyDeliveryOptionsPage();
	}
}
