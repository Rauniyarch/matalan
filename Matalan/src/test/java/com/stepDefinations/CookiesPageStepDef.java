package com.stepDefinations;

import com.runner.BaseClass;
import cucumber.api.java.en.Then;

public class CookiesPageStepDef extends BaseClass{
	
	@Then("^I should be directed to Cookies Info Page$")
	public void i_should_be_directed_to_Cookies_Info_Page() throws Throwable {
		cookiesPage.verifyCookiesPage();
	}
}
