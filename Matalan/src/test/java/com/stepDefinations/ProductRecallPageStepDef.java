package com.stepDefinations;

import com.runner.BaseClass;

import cucumber.api.java.en.Then;

public class ProductRecallPageStepDef extends BaseClass {

	@Then("^I should be directed to Product Recall Page$")
	public void i_should_be_directed_to_Product_Recall_Page() throws Throwable {
		productRecallPage.verifyProductRecallPage();
	}
}
