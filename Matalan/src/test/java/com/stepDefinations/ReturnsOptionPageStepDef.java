package com.stepDefinations;

import com.runner.BaseClass;
import cucumber.api.java.en.Then;

public class ReturnsOptionPageStepDef extends BaseClass{
	
	@Then("^I should be directed to Returns Info page$")
	public void i_should_be_directed_to_Returns_Info_page() throws Throwable {
		deliveryOptionsPage.verifyDeliveryOptionsPage();
	}

}
