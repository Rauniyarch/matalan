package com.stepDefinations;

import com.runner.BaseClass;
import cucumber.api.java.en.Then;

public class SearchResultPageStepDef extends BaseClass{
	
	@Then("^I should see valid \"([^\"]*)\"$")
	public void i_should_see_valid(String searchResults) throws Throwable {
		searchResultPage.verifySearchResultPage(searchResults);
	}
	
	@Then("^I should see error message$")
	public void i_should_see_error_message() throws Throwable {
		searchResultPage.verifyInvlaidSearchResultPage();
	}
	
	
	
}
