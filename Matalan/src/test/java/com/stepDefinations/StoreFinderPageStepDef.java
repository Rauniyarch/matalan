package com.stepDefinations;

import com.runner.BaseClass;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StoreFinderPageStepDef extends BaseClass {
	
	@Then("^I should be directed to Store Finder Page$")
	public void i_should_be_directed_to_Store_Finder_Page() throws Throwable {
		storeFinderPage.verifyStoreFinderPage();
	}
	
	@Given("^I am in StoreFinderPage$")
	public void i_am_in_StoreFinderPage() throws Throwable {
		storeFinderPage.verifyStoreFinderPage();
	}
	
	@Given("^I enter valid Detail in search field \"([^\"]*)\"$")
	public void i_enter_valid_Detail_in_search_field(String StoreSearchData) throws Throwable {
		storeFinderPage.searchStore(StoreSearchData);
	}
	
	@Then("^I should see stores near that Detail$")
	public void i_should_see_stores_near_that_Detail() throws Throwable {
		storeFinderPage.verifySearchedStores();
	}
	
	@When("^I enter valid Detail in search field \"([^\"]*)\" and click on show more stores$")
	public void i_enter_valid_Detail_in_search_field_and_click_on_show_more_stores(String tw3) throws Throwable {
		storeFinderPage.verifyStoreSearchWithKeyword(tw3);  
	}

	@Then("^I should see list of all other stores$")
	public void i_should_see_list_of_all_other_stores() throws Throwable {
		storeFinderPage.seeAllOtherStores();
	}
	
	@When("^I enter Invalid Detail \"([^\"]*)\"$")
	public void i_enter_Invalid_Detail(String InvalidSearchData) throws Throwable {
		storeFinderPage.verifyStoreLocatorInvalidData(InvalidSearchData);
	}
	
	@Then("^I should see Error Message$")
	public void i_should_see_Error_Message() throws Throwable {
		storeFinderPage.verifyStoreLocatorResultForInvalidData();
	}
	
	
	
	
}
