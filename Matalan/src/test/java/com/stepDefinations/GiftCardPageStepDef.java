package com.stepDefinations;

import com.runner.BaseClass;
import cucumber.api.java.en.Then;

public class GiftCardPageStepDef extends BaseClass{
	
	@Then("^I should be directed to Gift Cards Page$")
	public void i_should_be_directed_to_Gift_Cards_Page() throws Throwable {
		giftCardPage.verifyGiftCardPage();
	}
}
