package com.stepDefinations;

import com.runner.BaseClass;

import cucumber.api.java.en.Then;

public class AcceptabelUsePolicyPageStepDef extends BaseClass {
	
	@Then("^I should be directed to Acceptable Use Policy Info Page$")
	public void i_should_be_directed_to_Acceptable_Use_Policy_Info_Page() throws Throwable {
		acceptabelUsePolicyPage.verifyAcceptabelUsePolicyPage();
	}
}
