package com.stepDefinations;

import com.runner.BaseClass;
import cucumber.api.java.en.Then;

public class SavedItemPageStepDef extends BaseClass{

	@Then("^I should be directed to saved-items page and see saved products$")
	public void i_should_be_directed_to_saved_items_page_and_see_saved_products() throws Throwable {
	    savedItemPage.verifySavedItemPage();
	}

	
}
