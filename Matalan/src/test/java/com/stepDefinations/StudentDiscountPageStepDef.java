package com.stepDefinations;

import com.runner.BaseClass;
import cucumber.api.java.en.Then;

public class StudentDiscountPageStepDef extends BaseClass {

	@Then("^I should be directed to Student Discount Info page$")
	public void i_should_be_directed_to_Student_Discount_Info_page() throws Throwable {
		studentDiscountPage.verifyStudentDiscountPage();
	}
}
