package com.stepDefinations;

import com.runner.BaseClass;
import cucumber.api.java.en.Then;

public class CharityPageStepDef extends BaseClass{
	
	@Then("^I should be directed to Charity Info Page$")
	public void i_should_be_directed_to_Charity_Info_Page() throws Throwable {
		charityPage.verfifyChartityPage();
	}

}
