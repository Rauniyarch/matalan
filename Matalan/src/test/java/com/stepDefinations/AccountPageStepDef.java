package com.stepDefinations;

import com.runner.BaseClass;
import cucumber.api.java.en.Then;

public class AccountPageStepDef extends BaseClass{
	
	@Then("^I Should be directed to Account/Login page$")
	public void i_Should_be_directed_to_Account_Login_page() throws Throwable {
	   accountPage.verifyAccountPage();
	}
}
