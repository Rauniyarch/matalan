package com.stepDefinations;

import com.runner.BaseClass;
import cucumber.api.java.en.Then;

public class AboutMatalanPageStepDef extends BaseClass{
	
	@Then("^I should be directed to About Matalan Info page$")
	public void i_should_be_directed_to_About_Matalan_Info_page() throws Throwable {
		aboutMatalanPage.verifyAboutMatalanPage();
	}
}
