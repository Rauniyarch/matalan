package com.stepDefinations;

import com.runner.BaseClass;

import cucumber.api.java.en.Then;

public class SitemapPageStepDef extends BaseClass {
	
	@Then("^I should be directed to Sitemap Page$")
	public void i_should_be_directed_to_Sitemap_Page() throws Throwable {
		sitemapPage.verifySitemapPage();
	}
}
