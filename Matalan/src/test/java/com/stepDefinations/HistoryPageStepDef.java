package com.stepDefinations;

import com.runner.BaseClass;
import cucumber.api.java.en.Then;

public class HistoryPageStepDef extends BaseClass{
	
	@Then("^I should be directed to Matalan History Info Page$")
	public void i_should_be_directed_to_Matalan_History_Info_Page() throws Throwable {
		historyPage.verifyHistorypage();
	}
}
