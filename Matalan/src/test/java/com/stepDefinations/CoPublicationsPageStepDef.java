package com.stepDefinations;

import com.runner.BaseClass;
import cucumber.api.java.en.Then;

public class CoPublicationsPageStepDef extends BaseClass{
	
	@Then("^I should be directed to Corporate Publications Info Page$")
	public void i_should_be_directed_to_Corporate_Publications_Info_Page() throws Throwable {
		coPublicationsPage.verifyCoPublicationsPage();
	}
}
