package com.stepDefinations;

import com.runner.BaseClass;
import cucumber.api.java.en.Then;

public class FAQPageStepDef extends BaseClass{
	
	@Then("^I should be directed to customer service FAQs page$")
	public void i_should_be_directed_to_customer_service_FAQs_page() throws Throwable {
		fAQPage.verifyFAQPage();
	}
}
