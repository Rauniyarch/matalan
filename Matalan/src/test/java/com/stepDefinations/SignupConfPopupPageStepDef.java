package com.stepDefinations;

import com.runner.BaseClass;

import cucumber.api.java.en.Then;

public class SignupConfPopupPageStepDef extends BaseClass{
	
	@Then("^I should see the confirmation box with recaptcha$")
	public void i_should_see_the_confirmation_box_with_recaptcha() throws Throwable {
		signupConfPopupPage.verifySignupConfPopup();
	}
}
