package com.stepDefinations;

import com.runner.BaseClass;

import cucumber.api.java.en.Then;

public class JobsPageStepDef extends BaseClass{
	
	@Then("^I should be directed to Jobs Search Page$")
	public void i_should_be_directed_to_Jobs_Search_Page() throws Throwable {
		jobsPage.verifyJobsPage();
	}

}
