package com.stepDefinations;

import com.runner.BaseClass;
import cucumber.api.java.en.Then;

public class CartPageStepDef extends BaseClass {
	
	@Then("^I should be directed to cart page$")
	public void i_should_be_directed_to_cart_page() throws Throwable {
	    cartPage.verifyCartPage();
	}
}
