package com.stepDefinations;

import com.runner.BaseClass;
import cucumber.api.java.en.Then;

public class SocialMediaPageStepDef extends BaseClass{
	
	@Then("^I should be directed to Twitter page of Matalan external Page$")
	public void i_should_be_directed_to_Twitter_page_of_Matalan_external_Page() throws Throwable {
		socialMediaPage.verifyTwitter();
	}
	
	@Then("^I should be directed to Instagram page of Matalan external Page$")
	public void i_should_be_directed_to_Instagram_page_of_Matalan_external_Page() throws Throwable {
	   socialMediaPage.verifyInstagram();
	}

	@Then("^I should be directed to YouTube page of Matalan external Page$")
	public void i_should_be_directed_to_YouTube_page_of_Matalan_external_Page() throws Throwable {
		socialMediaPage.verifyYoutube();
	}
	
	@Then("^I should be directed to Facebook page of Matalan external Page$")
	public void i_should_be_directed_to_Facebook_page_of_Matalan_external_Page() throws Throwable {
		socialMediaPage.verifyFacebook();
	}
	
	@Then("^I should be directed to Pinterest page of Matalan external Page$")
	public void i_should_be_directed_to_Pinterest_page_of_Matalan_external_Page() throws Throwable {
		socialMediaPage.verifyPinterest();
	}

	
}
