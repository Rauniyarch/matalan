package com.stepDefinations;

import com.runner.BaseClass;

import cucumber.api.java.en.Then;

public class PrivacyPolicyPageStepDef extends BaseClass {
	
	@Then("^I should be directed to Privacy Policy Info Page$")
	public void i_should_be_directed_to_Privacy_Policy_Info_Page() throws Throwable {
		privacyPolicyPage.verifyPrivacyPolicyPage();
	}
}
