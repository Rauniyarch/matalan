package com.stepDefinations;

import com.runner.BaseClass;
import cucumber.api.java.en.Then;

public class CoronavirusPageStepDef extends BaseClass{
	
	@Then("^I should be directed to Coronavirus Info Page$")
	public void i_should_be_directed_to_Coronavirus_Info_Page() throws Throwable {
		coronavirusPage.verifyCoronavirusPage();
	}
}
