package com.stepDefinations;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

//import org.openqa.selenium.support.ui.WebDriverWait;
import com.runner.BaseClass;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks extends BaseClass {
	
	@Before
	public void start() {
		//System.setProperty("webdriver.chrome.driver", "/Users/chandan/Desktop/Automation/chromedriver");
		//driver = new ChromeDriver();
			
		System.setProperty("webdriver.gecko.driver", "/Users/chandan/Desktop/Automation/geckodriver");
		driver=new FirefoxDriver();
		driver.get("https://www.matalan.co.uk/");
		driver.findElement(By.cssSelector(".c-cookie-banner__close .icon-ui-cross")).click();
		
	}

	@After
	public void close() {
		driver.close();
	}
}
