package com.stepDefinations;

import com.runner.BaseClass;
import cucumber.api.java.en.Then;

public class ModernSlaveryPageStepDef extends BaseClass{
	
	@Then("^I should be directed to Modern Slavery Act Statement Page$")
	public void i_should_be_directed_to_Modern_Slavery_Act_Statement_Page() throws Throwable {
		modernSlaveryPage.verifyModernSlaverypage();
	}

}
