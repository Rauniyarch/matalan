package com.stepDefinations;

import com.runner.BaseClass;

import cucumber.api.java.en.Then;

public class HelpPageStepDef extends BaseClass{
	
	@Then("^I should see Help Page$")
	public void i_should_see_Help_Page() throws Throwable {
		helpPage.verifyContactHelpPage();
	}
}
