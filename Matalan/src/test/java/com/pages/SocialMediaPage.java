package com.pages;

import org.junit.Assert;


import com.runner.BaseClass;

public class SocialMediaPage extends BaseClass{
	
	public void verifyTwitter() {
		
		for (String Twitterpage:driver.getWindowHandles()) 
		{
			driver.switchTo().window(Twitterpage);
		}
		Assert.assertEquals("https://twitter.com/matalan", driver.getCurrentUrl());
	}
	
	public void verifyInstagram() {
				
		for (String InstgramPage:driver.getWindowHandles())
		{
			driver.switchTo().window(InstgramPage);
		}
		Assert.assertEquals("https://www.instagram.com/shopmatalan/", driver.getCurrentUrl());
	}
	
	public void verifyYoutube() {
		for (String Youtubepage:driver.getWindowHandles())
		{
			driver.switchTo().window(Youtubepage);
		}
		Assert.assertEquals("https://www.youtube.com/user/MatalanFashion", driver.getCurrentUrl());
	}
	
	public void verifyFacebook() {
		for (String FacebookPage:driver.getWindowHandles())
		{
			driver.switchTo().window(FacebookPage);
		}
		Assert.assertEquals("https://www.facebook.com/shopmatalan/", driver.getCurrentUrl());
	}
	
	public void verifyPinterest() {
		for (String Pinterest:driver.getWindowHandles()) {
			driver.switchTo().window(Pinterest);
		}
		Assert.assertEquals("https://www.pinterest.co.uk/matalan/", driver.getCurrentUrl());
	}
	
}
