package com.pages;

import org.junit.Assert;

import com.runner.BaseClass;

public class GiftCardPage extends BaseClass{
	
	public void verifyGiftCardPage() {
		Assert.assertEquals("https://www.matalan.co.uk/gifts/gift-cards", driver.getCurrentUrl());
	}
}
