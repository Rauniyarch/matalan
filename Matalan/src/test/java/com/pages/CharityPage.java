package com.pages;

import org.junit.Assert;

import com.runner.BaseClass;

public class CharityPage extends BaseClass {
	public void verfifyChartityPage() {
		Assert.assertEquals("https://www.matalan.co.uk/charity", driver.getCurrentUrl());
	}
}
