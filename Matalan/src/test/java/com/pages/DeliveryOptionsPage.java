package com.pages;

import org.junit.Assert;
import com.runner.BaseClass;

public class DeliveryOptionsPage extends BaseClass{
	
	public void verifyDeliveryOptionsPage () {
		Assert.assertEquals("https://www.matalan.co.uk/customer-services/delivery", driver.getCurrentUrl());
	}
}
