package com.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
//import org.openqa.selenium.JavascriptExecutor;
//import org.openqa.selenium.interactions.Actions;
//import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.runner.BaseClass;

public class HomePage extends BaseClass{
	
	public static By HOMEPAGELOGO = By.cssSelector(".o-header__brand");
	public static By HOMEPAGESAVEDICON = By.cssSelector("a[data-behavior='view_wishlist_button_desktop']");
	public static By HOMEPAGEVIEWBAGLINK = By.cssSelector("a[data-behavior='mini_basket_link']");
	public static By HOMEPAGEMYACCOUNTLINK = By.cssSelector("a[analytics_re='Header-_-Top Nav-_-My Account']");
	public static By HOMEPAGEDELIVERYLINK = By.cssSelector("a[analytics_re='Header-_-Top Nav-_-Delivery']\"");
	public static By HOMEPAGERETURNSLINK = By.cssSelector("a[analytics_re='Header-_-Top Nav-_-Returns']\"");
	public static By HOMEPAGESTOREFINDERLINK = By.cssSelector("a[analytics_re='Header-_-Top Nav-_-Store Finder']");
	public static By HOMEPAGECONTACTLINK = By.cssSelector("a[analytics_re='Header-_-Top Nav-_-Contact']");
	public static By GOTITCOOKIESBTN = By.cssSelector(".c-cookie-banner__btn");
	public static By CANCELCOOKIESBTN = By.cssSelector(".c-cookie-banner__close .icon-ui-cross");
	
	public static By SIGNUPEMAIL= By.cssSelector("input[data-content='Please enter a valid email address']");
	public static By SIGNUPEMAILBTN = By.cssSelector("button[data-validate='email']");
	public static By FAQLINKICONFOOTER= By.cssSelector("a[analytics_re='Footer-_-Menu-_-Store Finder']");
	public static By STOREFINDERFOOTER= By.cssSelector("a[analytics_re='Footer-_-Menu-_-Store Finder']");
	public static By TRACKMYORERFOOTER= By.cssSelector("a[analytics_re='Footer-_-Menu-_-Track My Order']");
	public static By HERETOHELPFOOTER= By.cssSelector("a[analytics_re='Footer-_-Menu-_-Here to Help']");
	public static By GIFTCARDFOOTER= By.cssSelector("a[analytics_re='Footer-_-Menu-_-Gift Cards']");
	public static By DELIVERYLINKFOOTER= By.cssSelector("a[analytics_re='Footer-_-Menu-_-Delivery']");
	public static By RETURNLINKFOOTER = By.cssSelector("a[analytics_re='Footer-_-Menu-_-Returns']");
	public static By PRODUCTRECALLLINKFOOTER= By.cssSelector("a[analytics_re='Footer-_-Menu-_-Product Recall']");
	public static By FAQLINKFOOTER= By.cssSelector("a[analytics_re='Footer-_-Menu-_-FAQs']");
	public static By STUDENTDISCOUNTFOOTER= By.cssSelector("a[analytics_re='Footer-_-Menu-_-Student Discount']");
	public static By ABOUTLINKFOOTER= By.cssSelector("a[analytics_re='Footer-_-Menu-_-About']");
	public static By CORONAVIRUSLINKFOOTER= By.cssSelector("a[analytics_re='Footer-_-Menu-_-Coronavirus']");
	public static By HISTORYLINKFOOTER= By.cssSelector("a[analytics_re='Footer-_-Menu-_-History']");
	public static By JOBSLINKFOOTER= By.cssSelector("a[analytics_re='Footer-_-Menu-_-Jobs']");
	public static By MODERNSLAVARYACTFOOTER= By.cssSelector("a[analytics_re='Footer-_-Menu-_-Modern Slavery Act']");
	public static By CORPPUBFOOTER= By.cssSelector("a[analytics_re='Footer-_-Menu-_-Corporate Publications']");
	public static By CHARITYLINKFOOTER= By.cssSelector("a[analytics_re='Footer-_-Menu-_-Charity']");
	public static By COOKIESLINKFOOTER= By.cssSelector("a[analytics_re='Footer-_-Menu-_-Cookies']");
	public static By PRIVACYPOLICYFOOTER= By.cssSelector("a[analytics_re='Footer-_-Menu-_-Privacy Policy']");
	
	public static By TERMSCONDITION = By.cssSelector("a[analytics_re='Footer-_-Menu-_-Terms & Conditions']");
	public static By ACCEPTABELUSEPOLICY = By.cssSelector("a[analytics_re='Footer-_-Menu-_-Acceptable Use Policy']");
	public static By SITEMAP = By.cssSelector("a[analytics_re='Footer-_-Menu-_-Sitemap']");
	public static By TWITTER = By.cssSelector("a[analytics_re='Footer-_-Social-_-Twitter']");
	public static By INSTAGRAM = By.cssSelector("a[analytics_re='Footer-_-Social-_-Instagram']");
	public static By YOUTUBE = By.cssSelector("a[analytics_re='Footer-_-Social-_-Youtube']");
	public static By FACEBOOK = By.cssSelector("a[analytics_re='Footer-_-Social-_-Facebook']");
	public static By PINTEREST = By.cssSelector("a[analytics_re='Footer-_-Social-_-Pinterest']");
	
	
	
	public static WebDriverWait wait; 
	
	//public static By  = By.cssSelector("");
	//public static By  = By.cssSelector("");

	
	public void verifyHomePageLogo() {
		Assert.assertEquals("https://www.matalan.co.uk/", driver.getCurrentUrl()); 
	}
	
	public void clickHomePageLogo() {
		driver.findElement(HOMEPAGELOGO).click();
	}
	
	public void homePageSavedIcon() {
		driver.findElement(HOMEPAGESAVEDICON).click();
	}
	
	public void viewBagLink () {
		driver.findElement(HOMEPAGEVIEWBAGLINK).click();
	}
	
	public void myAccountLink() {
		driver.findElement(HOMEPAGEMYACCOUNTLINK).click();
	}
	
	public void deliveryOptionsLink() {
		driver.findElement(HOMEPAGEDELIVERYLINK).click();
	}
	
	public void returnsOptionLink() {
		driver.findElement(HOMEPAGERETURNSLINK).click();
	}
	
	public void storeFinderLink() {
		driver.findElement(HOMEPAGESTOREFINDERLINK).click();
	}
	
	public void contactLink() {
		driver.findElement(HOMEPAGECONTACTLINK).click();
	}
	
	public void gotItCookiesPolicy() {
		driver.findElement(GOTITCOOKIESBTN).click();
	}
	
	public void disappearCookiePopup() {
		Assert.assertEquals("cart--is-empty",driver.manage().getCookieNamed("cart--is-empty"));
	}
	
	public void cancelCookiePolicy() {
		driver.findElement(CANCELCOOKIESBTN).click();
	}
	
	public void signUpValidEmailAddress() {
		driver.findElement(SIGNUPEMAIL).clear();
		driver.findElement(SIGNUPEMAIL).sendKeys("chandan_rauniyar2002@yahoo.com");
		driver.findElement(SIGNUPEMAILBTN).click();
	}
	
	public void signUpInvalidEmailAddress() {
		driver.findElement(SIGNUPEMAIL).clear();
		driver.findElement(SIGNUPEMAIL).sendKeys("chandan_rauniyar@yahoo.com");
		driver.findElement(SIGNUPEMAILBTN).click();
	}
	
	public void verifyStoreFinderIconFooter() {
		driver.findElements(STOREFINDERFOOTER).get(0).click();
	}
	
	public void veriftFAQLinkIconFooter() {
		driver.findElements(FAQLINKICONFOOTER).get(1).click();
	}
	
	public void verifyTrackMyOrderLink() {
		driver.findElement(TRACKMYORERFOOTER).click();
	}
	
	public void verifyHereToHelpLink() {
		driver.findElement(HERETOHELPFOOTER).click();
	}
	
	public void verifyGiftCardLink() {
		driver.findElement(GIFTCARDFOOTER).click();
	}
	
	public void verifyFooterDeliveryLink() {
		driver.findElement(DELIVERYLINKFOOTER).click();
	}
	
	public void verifyFooterReturnsLink() {
		driver.findElement(RETURNLINKFOOTER).click();
	}
	
	public void verifyFooterProductRecallLink() {
		driver.findElement(PRODUCTRECALLLINKFOOTER).click();
	}
	
	public void verifyFooterFAQLink() {
		driver.findElement(FAQLINKFOOTER).click();
	}
	
	public void verifyFooterStudentDiscountLink() {
		driver.findElement(STUDENTDISCOUNTFOOTER).click();
	}
	
	public void verifyAboutLinkFooter() throws InterruptedException {
		driver.findElement(ABOUTLINKFOOTER).click();
	}
	
	public void verifyCoronavirusLinkFooter() {
		driver.findElement(CORONAVIRUSLINKFOOTER).click();
	}
	
	public void verifyHistoryLinkFooter() {
		driver.findElement(HISTORYLINKFOOTER).click();
	}
	
	public void verifyJobsLinkFooter() {
		driver.findElement(JOBSLINKFOOTER).click();
	}
	public void verifyModernSlaveryActLinkFooter() {
		driver.findElement(MODERNSLAVARYACTFOOTER).click();
	}
	
	public void verifyCoPublicationsLinkFooter() {
		driver.findElement(CORPPUBFOOTER).click();
	}
	
	public void verifyCharityLinkFooter() {
		driver.findElement(CHARITYLINKFOOTER).click();
	}
	
	public void verifyCookiesLinkFooter() {
		driver.findElement(COOKIESLINKFOOTER).click();
	}
	public void verifyPrivacyPolicyLinkFooter() {
		driver.findElement(PRIVACYPOLICYFOOTER).click();
	}
	public void verifyTermsConditionsLinkFooter() {
		driver.findElement(TERMSCONDITION).click();
	}
	public void verifyAcceptableUsePolicyLinkFooter() {
		driver.findElement(ACCEPTABELUSEPOLICY).click();
	}
	public void verifySitemapPage() {
		driver.findElement(SITEMAP).click();
	}
	
	public void verifyTwitterIcon() throws InterruptedException {
		driver.findElement(TWITTER).click();
	}
	public void verifyInstagramIcon() {
		driver.findElement(INSTAGRAM).click();
	}
	public void verifyYoutubeIcon() {
		driver.findElement(YOUTUBE).click();
	}
	public void verifyFacebookIcon() {
		driver.findElement(FACEBOOK).click();
	}
	public void verifyPinterestICon() {
		driver.findElement(PINTEREST).click();
	}
	
	public void verifySearchwithValidSearchInputs(String SearchTerm) {
		driver.findElement(By.cssSelector("input[analytics_re='Header-_-Search-_-Input']")).clear();
		driver.findElement(By.cssSelector("input[analytics_re='Header-_-Search-_-Input']")).sendKeys(SearchTerm);
		driver.findElements(By.cssSelector("button[data-action='submit_search']")).get(0).click();
	}
	
	public void verifySearchwithInvalidSearchInputs(String invalidSearchTerm) {
		driver.findElement(By.cssSelector("input[analytics_re='Header-_-Search-_-Input']")).clear();
		driver.findElement(By.cssSelector("input[analytics_re='Header-_-Search-_-Input']")).sendKeys(invalidSearchTerm);
		driver.findElements(By.cssSelector("button[data-action='submit_search']")).get(0).click();
	}
	
}
