package com.pages;

import org.junit.Assert;

import com.runner.BaseClass;

public class SitemapPage extends BaseClass {
	public void verifySitemapPage() {
		Assert.assertEquals("https://www.matalan.co.uk/sitemap", driver.getCurrentUrl());
	}
}
