package com.pages;

import org.junit.Assert;

import com.runner.BaseClass;

public class CoronavirusPage extends BaseClass {
	public void verifyCoronavirusPage() {
		Assert.assertEquals("https://www.matalan.co.uk/coronavirus", driver.getCurrentUrl());
	}
}
