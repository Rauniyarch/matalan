package com.pages;

import org.junit.Assert;

import com.runner.BaseClass;

public class CoPublicationsPage extends BaseClass{
	public void verifyCoPublicationsPage() {
		Assert.assertEquals("https://www.matalan.co.uk/corporate/corporate-publications", driver.getCurrentUrl());
	}
	
}
