package com.pages;

import org.junit.Assert;
import com.runner.BaseClass;

public class ProductRecallPage extends BaseClass{
	
	public void verifyProductRecallPage() {
	Assert.assertEquals("https://www.matalan.co.uk/customer-services/product-recall-notice", driver.getCurrentUrl());
	}
}
