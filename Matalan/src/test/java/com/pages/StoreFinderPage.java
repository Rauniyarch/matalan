package com.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.runner.BaseClass;

public class StoreFinderPage extends BaseClass{
	
	
	public void verifyStoreFinderPage() {
		Assert.assertEquals("https://www.matalan.co.uk/stores", driver.getCurrentUrl());
	}
	
	public void searchStore(String StoreSearchData) {
		driver.findElement(By.cssSelector("#addressEntry")).clear();
		driver.findElement(By.cssSelector("#addressEntry")).sendKeys(StoreSearchData);
		driver.findElement(By.cssSelector("#findStore")).click();
		
	}
	
	public void verifySearchedStores() throws InterruptedException {
		//Thread.sleep(2000);
		WebDriverWait wait = new WebDriverWait (driver, 10);
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("h4.u-mar-t-medium")));
		Assert.assertEquals("Your stores", driver.findElement(By.cssSelector("h4.u-mar-t-medium")).getText());
}
	
	public void verifyStoreSearchWithKeyword(String tw3) {
		driver.findElement(By.cssSelector("#addressEntry")).clear();
		driver.findElement(By.cssSelector("#addressEntry")).sendKeys(tw3);
		driver.findElement(By.cssSelector("#findStore")).click();
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".js-show-more-stores")));
		driver.findElement(By.cssSelector(".js-show-more-stores")).click();
		
	}
	
	public void seeAllOtherStores() {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("h5 .js-store-numbers")));
		Assert.assertEquals("235", driver.findElement(By.cssSelector("h5 .js-store-numbers")).getText());
	}
	
	public void verifyStoreLocatorInvalidData(String InvalidSearchData) {
		driver.findElement(By.cssSelector("#addressEntry")).clear();
		driver.findElement(By.cssSelector("#addressEntry")).sendKeys(InvalidSearchData);
		driver.findElement(By.cssSelector("#findStore")).click();
	}
	
	
	public void verifyStoreLocatorResultForInvalidData () {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".o-form__label--message")));
		Assert.assertEquals("Please enter a valid address, city or postcode", driver.findElement(By.cssSelector(".o-form__label--message")).getText());
	}
	
	
	
	
	
	
	
	
}

