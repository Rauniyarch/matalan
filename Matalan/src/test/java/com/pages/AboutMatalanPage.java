package com.pages;

import org.junit.Assert;

import com.runner.BaseClass;

public class AboutMatalanPage extends BaseClass{
	
	public void verifyAboutMatalanPage() {
		Assert.assertEquals("https://www.matalan.co.uk/corporate", driver.getCurrentUrl());
	}
}
