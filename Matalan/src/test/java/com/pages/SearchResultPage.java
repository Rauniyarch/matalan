package com.pages;

import org.junit.Assert;
import org.openqa.selenium.By;

import com.runner.BaseClass;

public class SearchResultPage extends BaseClass {
		
	public void verifySearchResultPage(String SearchTerm) {
		Assert.assertEquals(SearchTerm, driver.getTitle());
	}
	
	public void verifyInvlaidSearchResultPage() {
		Assert.assertEquals("To help with your search why not try:", driver.findElement(By.cssSelector("h4")).getText());
	}
	
}
