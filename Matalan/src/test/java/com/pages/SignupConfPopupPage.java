package com.pages;

import org.junit.Assert;
import org.openqa.selenium.By;

import com.runner.BaseClass;

public class SignupConfPopupPage extends BaseClass{
		
		public void verifySignupConfPopup() {
			Assert.assertEquals("Confirmation", driver.findElement(By.cssSelector(".modal-title")).getText());
	}
		
}
