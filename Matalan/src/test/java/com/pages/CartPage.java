package com.pages;

import org.junit.Assert;

import com.runner.BaseClass;

public class CartPage extends BaseClass{

	public void verifyCartPage() {
		Assert.assertEquals("https://www.matalan.co.uk/cart", driver.getCurrentUrl());
	}
}
