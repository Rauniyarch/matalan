package com.pages;

import org.junit.Assert;
import com.runner.BaseClass;

public class StudentDiscountPage extends BaseClass{
	
	public void verifyStudentDiscountPage() {
		Assert.assertEquals("https://www.matalan.co.uk/student-discounts", driver.getCurrentUrl());
	}
}
