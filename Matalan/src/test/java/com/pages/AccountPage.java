package com.pages;

import org.junit.Assert;

import com.runner.BaseClass;

public class AccountPage extends BaseClass {
	
	public void verifyAccountPage() {
		Assert.assertEquals("https://www.matalan.co.uk/account/login?return_path=%2Faccount%2Fedit", driver.getCurrentUrl() );
	}
}
