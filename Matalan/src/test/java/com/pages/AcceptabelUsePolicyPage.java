package com.pages;

import org.junit.Assert;

import com.runner.BaseClass;

public class AcceptabelUsePolicyPage extends BaseClass {
	public void verifyAcceptabelUsePolicyPage() {
		Assert.assertEquals("https://www.matalan.co.uk/corporate/acceptable-use-policy", driver.getCurrentUrl());
	}
}
