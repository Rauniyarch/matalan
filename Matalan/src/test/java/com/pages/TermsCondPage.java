package com.pages;

import org.junit.Assert;

import com.runner.BaseClass;

public class TermsCondPage extends BaseClass{
	public void verifyTermsCondPage() {
		Assert.assertEquals("https://www.matalan.co.uk/customer-services/terms", driver.getCurrentUrl());
	}
}
