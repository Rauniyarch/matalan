package com.pages;

import org.junit.Assert;

import com.runner.BaseClass;

public class SavedItemPage extends BaseClass{

	public void verifySavedItemPage() {
		Assert.assertEquals("https://www.matalan.co.uk/saved-items", driver.getCurrentUrl());
	}
}
