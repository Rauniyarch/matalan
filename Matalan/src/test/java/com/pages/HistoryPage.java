package com.pages;

import org.junit.Assert;

import com.runner.BaseClass;

public class HistoryPage extends BaseClass{
	
	public void verifyHistorypage() {
	Assert.assertEquals("https://www.matalan.co.uk/history", driver.getCurrentUrl());	
	}
}
