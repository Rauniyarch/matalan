package com.pages;

import org.junit.Assert;

import com.runner.BaseClass;

public class HelpPage extends BaseClass{
	
	public void verifyContactHelpPage() {
		Assert.assertEquals("https://www.matalan.co.uk/customer-services/here-to-help", driver.getCurrentUrl());
	}
	
	
}
