package com.pages;

import org.junit.Assert;

import com.runner.BaseClass;

public class ModernSlaveryPage extends BaseClass{
	
	public void verifyModernSlaverypage() {
		Assert.assertEquals("https://www.matalan.co.uk/corporate/modern-slavery-act-statement", driver.getCurrentUrl());
	}
}
