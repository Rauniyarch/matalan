package com.pages;

import org.junit.Assert;

import com.runner.BaseClass;

public class JobsPage extends BaseClass{
	public void verifyJobsPage() {
		Assert.assertEquals("https://www.matalan.jobs/", driver.getCurrentUrl());
	}
}
