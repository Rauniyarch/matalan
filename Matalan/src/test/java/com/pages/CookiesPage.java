package com.pages;

import org.junit.Assert;

import com.runner.BaseClass;

public class CookiesPage extends BaseClass{
	
	public void verifyCookiesPage() {
		Assert.assertEquals("https://www.matalan.co.uk/corporate/cookies", driver.getCurrentUrl());
	}
}
