package com.pages;

import org.junit.Assert;

import com.runner.BaseClass;

public class FAQPage extends BaseClass{
	
	public void verifyFAQPage() {
		Assert.assertEquals("https://www.matalan.co.uk/customer-services/faq", driver.getCurrentUrl());
	}
}
