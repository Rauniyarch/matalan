package com.pages;

import org.junit.Assert;

import com.runner.BaseClass;

public class PrivacyPolicyPage extends BaseClass {
	public void verifyPrivacyPolicyPage() {
		Assert.assertEquals("https://www.matalan.co.uk/customer-services/privacy", driver.getCurrentUrl());
	}
}
