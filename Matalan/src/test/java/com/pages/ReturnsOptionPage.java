package com.pages;

import org.junit.Assert;

import com.runner.BaseClass;

public class ReturnsOptionPage extends BaseClass{
	public void verifyReturnsOptionsPage() {
		Assert.assertEquals("https://www.matalan.co.uk/customer-services/returns-cancelations", driver.getCurrentUrl());
	}
}
