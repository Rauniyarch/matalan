package com.runner;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.pages.AboutMatalanPage;
import com.pages.AcceptabelUsePolicyPage;
import com.pages.AccountPage;
import com.pages.CartPage;
import com.pages.CharityPage;
import com.pages.CoPublicationsPage;
import com.pages.CookiesPage;
import com.pages.CoronavirusPage;
import com.pages.DeliveryOptionsPage;
import com.pages.FAQPage;
import com.pages.GiftCardPage;
import com.pages.HelpPage;
import com.pages.HistoryPage;
import com.pages.HomePage;
import com.pages.JobsPage;
import com.pages.ModernSlaveryPage;
import com.pages.PrivacyPolicyPage;
import com.pages.ProductRecallPage;
import com.pages.SavedItemPage;
import com.pages.SearchResultPage;
import com.pages.SignupConfPopupPage;
import com.pages.SitemapPage;
import com.pages.SocialMediaPage;
import com.pages.StoreFinderPage;
import com.pages.StudentDiscountPage;
import com.pages.TermsCondPage;

public class BaseClass {
	public static WebDriverWait wait; 
	
	
	public static WebDriver driver;
	public static HomePage homePage = new HomePage();
	public static SavedItemPage savedItemPage = new SavedItemPage();
	public static CartPage cartPage = new CartPage();
	public static AccountPage accountPage = new AccountPage();
	public static DeliveryOptionsPage deliveryOptionsPage = new DeliveryOptionsPage();
	public static StoreFinderPage storeFinderPage = new StoreFinderPage();
	public static HelpPage helpPage = new HelpPage();
	public static SignupConfPopupPage signupConfPopupPage = new SignupConfPopupPage();
	public static FAQPage fAQPage = new FAQPage();
	public static GiftCardPage giftCardPage = new GiftCardPage();
	public static ProductRecallPage productRecallPage = new ProductRecallPage();
	public static StudentDiscountPage studentDiscountPage = new StudentDiscountPage();
	public static AboutMatalanPage aboutMatalanPage = new AboutMatalanPage();
	public static CoronavirusPage coronavirusPage = new CoronavirusPage();
	public static HistoryPage historyPage = new HistoryPage();
	public static JobsPage jobsPage = new JobsPage();
	public static ModernSlaveryPage modernSlaveryPage = new ModernSlaveryPage();
	public static CoPublicationsPage coPublicationsPage = new CoPublicationsPage();
	public static CharityPage charityPage = new CharityPage();
	public static CookiesPage cookiesPage = new CookiesPage();
	public static PrivacyPolicyPage privacyPolicyPage = new PrivacyPolicyPage();
	public static TermsCondPage termsCondPage = new TermsCondPage();
	public static AcceptabelUsePolicyPage acceptabelUsePolicyPage = new AcceptabelUsePolicyPage();
	public static SitemapPage sitemapPage = new SitemapPage();
	public static SocialMediaPage socialMediaPage = new SocialMediaPage();
	public static SearchResultPage searchResultPage = new SearchResultPage();
	
	
}
